﻿using System;
using Newtonsoft.Json;
using System.Net.Http;
using CoolParking.UserInterface;
using System.Threading.Tasks;
using System.Collections.Generic;
using CoolParking.BL.Models;
using System.Text;
using System.Net;

namespace CoolParking.Client
{
    class Program
    {
        static HttpClient client;
        static void Main(string[] args)
        {
            client = new HttpClient();
            client.BaseAddress = new Uri("https://localhost:5001/api/");
            Run();
        }
        public static void Run()
        {
            /**LVL = 1**/
            while (true)
            {
                Console.Clear();
                var menu = new string[]{
                        "Parking balance",
                        "Parking capacity",
                        "Parking free places",
                        "List of vehicles",
                        "Vehicle by id(Example: AA-0001-AA)",
                        "Add vehicle",
                        "Remove vehicle",
                        "Last transactions",
                        "Log transactions",
                        "TopUp vehicle",
                        "Exit"
                    };
                ConsoleTextFormater.ShowMenuContent("PARKING SERVICE", menu);
                string point = ConsoleTextFormater.UserConsoleEnter("point");
                DoAction(point);
                ConsoleTextFormater.UserAwaiting();
            }
        }

        private static void DoAction(string point)
        {
            if (!point.Equals("0"))
                try
                {
                    HttpResponseMessage response = client.GetAsync("").Result;
                    //response.EnsureSuccessStatusCode();
                }
                catch(Exception)
                {
                    ConsoleTextFormater.ShowErrorMessage("No connection!");
                    return;
                }
            HttpResponseMessage result;
            switch (point)
            {
                case "0":
                    Environment.Exit(0);
                    break;
                case "1":
                    ConsoleTextFormater.ShowTopicMessage("Balance");
                    result = client.GetAsync("parking/balance").Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var balance = (decimal)JsonConvert.DeserializeObject(
                            result.Content.ReadAsStringAsync().Result, 
                            typeof(decimal)
                        );
                        ConsoleTextFormater.ShowMessage($"{balance}");
                    }
                    else
                    {
                        ConsoleTextFormater.ShowErrorMessage(result.StatusCode.ToString());
                    }
                    break;
                case "2":
                    ConsoleTextFormater.ShowTopicMessage("Capacity");
                    result = client.GetAsync("parking/capacity").Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var capacity = (int)JsonConvert.DeserializeObject(
                            result.Content.ReadAsStringAsync().Result,
                            typeof(int)
                        );
                        ConsoleTextFormater.ShowMessage($"{capacity}");
                    }
                    else
                    {
                        ConsoleTextFormater.ShowErrorMessage(result.StatusCode.ToString());
                    }
                    break;
                case "3":
                    ConsoleTextFormater.ShowTopicMessage("Free places");
                    result = client.GetAsync("parking/freePlaces").Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var freePlaces = (int)JsonConvert.DeserializeObject(
                            result.Content.ReadAsStringAsync().Result,
                            typeof(int)
                        );
                        ConsoleTextFormater.ShowMessage($"{freePlaces}");
                    }
                    else
                    {
                        ConsoleTextFormater.ShowErrorMessage(result.StatusCode.ToString());
                    }
                    break;
                case "4":
                    ConsoleTextFormater.ShowTopicMessage("Vehicles");
                    result = client.GetAsync("vehicles").Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var vehicles = JsonConvert.DeserializeObject(
                            result.Content.ReadAsStringAsync().Result,
                            typeof(List<Vehicle>)
                        ) as List<Vehicle>;
                        foreach (var veh in vehicles)
                            ConsoleTextFormater.ShowMessage(ConsoleTextFormater.GetVehicleInfo(veh));
                    }
                    else
                    {
                        ConsoleTextFormater.ShowErrorMessage(result.StatusCode.ToString());
                    }
                    break;
                case "5":
                    ConsoleTextFormater.ShowTopicMessage("Vehicle by ID");
                    string id = ConsoleTextFormater.UserConsoleEnter("vehicle id");
                    result = client.GetAsync($"vehicles/{id}").Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var vehicle = JsonConvert.DeserializeObject(
                            result.Content.ReadAsStringAsync().Result,
                            typeof(Vehicle)
                        ) as Vehicle;
                        ConsoleTextFormater.ShowMessage(vehicle.ToString());
                    }
                    else
                    {
                        ConsoleTextFormater.ShowErrorMessage(result.StatusCode.ToString());
                    }
                    break;
                case "6":
                    ConsoleTextFormater.ShowTopicMessage("Vehicles types");
                    for (int i = 1; i < 5; ++i)
                        ConsoleTextFormater.ShowMessage($"{i}. {(VehicleType)i}", addLeft: 4);
                    ConsoleTextFormater.ShowTopicMessage("Add vehicle");
                    try
                    {
                        string vid = ConsoleTextFormater.UserConsoleEnter("vehicle id");
                        int vtype = TryParseFunctions.TryReadUShort(ConsoleTextFormater.UserConsoleEnter("vehicle type"));
                        decimal vbalance = TryParseFunctions.TryReadDecimal(ConsoleTextFormater.UserConsoleEnter("balance"));
                        var content = new StringContent(
                            JsonConvert.SerializeObject(new {id = vid, vehicleType = vtype, balance = vbalance}), 
                            Encoding.UTF8, 
                            "application/json");
                        result = client.PostAsync($"vehicles", content).Result;
                        if (result.StatusCode == HttpStatusCode.Created)
                        {
                            Vehicle vehicle1 = JsonConvert.DeserializeObject(
                                result.Content.ReadAsStringAsync().Result,
                                typeof(Vehicle)
                            ) as Vehicle;
                            ConsoleTextFormater.ShowGoodMessage($"{result.StatusCode}");
                            ConsoleTextFormater.ShowMessage(vehicle1.ToString());
                        }
                        else
                        {
                            ConsoleTextFormater.ShowErrorMessage($"{result.StatusCode}");
                        }
                    }
                    catch(ArgumentException ex)
                    {
                        ConsoleTextFormater.ShowErrorMessage(ex.Message);
                    }
                    break;
                case "7":
                    ConsoleTextFormater.ShowTopicMessage("Remove vehicle");
                    try
                    {
                        string vid = ConsoleTextFormater.UserConsoleEnter("vehicle id");
                        result = client.DeleteAsync($"vehicles/{vid}").Result;
                        if (result.StatusCode == HttpStatusCode.NoContent)
                        {
                            ConsoleTextFormater.ShowGoodMessage($"{result.StatusCode}");
                            ConsoleTextFormater.ShowMessage($"Vehicle '{vid}' delete successful!");
                        }
                        else
                            ConsoleTextFormater.ShowErrorMessage($"{result.StatusCode}");
                    }
                    catch (ArgumentException ex)
                    {
                        ConsoleTextFormater.ShowErrorMessage(ex.Message);
                    }
                    break;
                case "8":
                    ConsoleTextFormater.ShowTopicMessage("Last transactions");
                    result = client.GetAsync($"transactions/last").Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var transactions = JsonConvert.DeserializeObject(
                            result.Content.ReadAsStringAsync().Result,
                            typeof(List<TransactionInfo>)
                        ) as List<TransactionInfo>;
                        foreach (var trans in transactions)
                            ConsoleTextFormater.ShowMessage(trans.ToString());
                    }
                    else
                    {
                        ConsoleTextFormater.ShowErrorMessage(result.StatusCode.ToString());
                    }
                    break;
                case "9":
                    ConsoleTextFormater.ShowTopicMessage("Log transactions");
                    result = client.GetAsync($"transactions/all").Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var logTransactions = JsonConvert.DeserializeObject(
                            result.Content.ReadAsStringAsync().Result,
                            typeof(string)
                        ) as string;
                        foreach (var trans in logTransactions.Split("\n"))
                            ConsoleTextFormater.ShowMessage(trans.ToString());
                    }
                    else
                    {
                        ConsoleTextFormater.ShowErrorMessage(result.StatusCode.ToString());
                    }
                    break;
                case "10":
                    ConsoleTextFormater.ShowTopicMessage("TopUp vehicle");
                    try
                    {
                        string vId = ConsoleTextFormater.UserConsoleEnter("vehicle ID");
                        decimal sum = TryParseFunctions.TryReadDecimal(ConsoleTextFormater.UserConsoleEnter("sum"));
                        var content = new StringContent(
                            JsonConvert.SerializeObject(new { id = vId, Sum = sum }),
                            Encoding.UTF8,
                            "application/json");
                        result =  client.PutAsync(
                            $"transactions/topUpVehicle",
                            content
                        ).Result;
                        if (result.IsSuccessStatusCode)
                        {
                            Vehicle vehicle1 = JsonConvert.DeserializeObject(
                                result.Content.ReadAsStringAsync().Result,
                                typeof(Vehicle)
                            ) as Vehicle;
                            ConsoleTextFormater.ShowGoodMessage($"{result.StatusCode}");
                            ConsoleTextFormater.ShowMessage(vehicle1.ToString());
                        }
                        else
                        {
                            ConsoleTextFormater.ShowErrorMessage($"{result.StatusCode}");
                        }
                    }
                    catch (ArgumentException ex)
                    {
                        ConsoleTextFormater.ShowErrorMessage(ex.Message);
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
