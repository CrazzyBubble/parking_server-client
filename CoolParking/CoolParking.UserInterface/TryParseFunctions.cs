﻿using System;
public static class TryParseFunctions
{
    public static decimal TryReadDecimal(string textValue)
    {
        decimal value;
        if (Decimal.TryParse(textValue.Trim(), out value))
            return value;
        throw new ArgumentException("Incorrect argument.");
    }

    public static double TryReadDouble(string textValue)
    {
        double value;
        if (Double.TryParse(textValue.Trim(), out value))
            return value;
        throw new ArgumentException("Incorrect argument.");
    }

    public static int TryReadUShort(string textValue)
    {
        int value;
        if (Int32.TryParse(textValue.Trim(), out value))
            return value;
        throw new ArgumentException("Incorrect argument.");
    }
    public static decimal TryReadDecimalPositive(string textValue)
    {
        var value = TryReadDecimal(textValue);
        if (value <= 0)
            throw new ArgumentException("Incorrect argument. Must be > 0.");
        return value;
    }

    public static double TryReadDoublePositive(string textValue)
    {
        var value = TryReadDouble(textValue);
        if (value <= 0)
            throw new ArgumentException("Incorrect argument. Must be > 0.");
        return value;
    }

    public static int TryReadUShortPositive(string textValue)
    {
        var value = TryReadUShort(textValue);
        if (value <= 0)
            throw new ArgumentException("Incorrect argument. Must be > 0.");
        return value;
    }
}
