﻿using System;
using CoolParking.BL.Services;
using CoolParking.BL.Models;
using System.IO;

namespace CoolParking.UserInterface
{
    class ConsoleUserInterface
    {
        private string _logFilePath;
        private TimerService withdrawTimer;
        private TimerService logTimer;
        private LogService logService;
        private ParkingService parkingService;
        private short LVL = 0; // Show where we are (what menu is)
        public ConsoleUserInterface(string path)
        {
            _logFilePath = path;
        }

        public void Run()
        {
            /**LVL=0**/
            while(LVL==0)
            {
                Console.Clear();
                ConsoleTextFormater.ShowMenuContent("Menu", new string[]{
                    "1. Start",
                    "2. Settings",
                    "3. Exit"
                });
                string point = ConsoleTextFormater.UserConsoleEnter("point");
                DoMenu(point);
            }
        }

        private void StartService()
        {
            /** Main work. Show menu to work with ParkingService. **/
            withdrawTimer = new TimerService(Settings.PaymentPeriod);
            logTimer = new TimerService(Settings.LogWritePeriod);
            logService = new LogService(_logFilePath);
            using (parkingService = new ParkingService(withdrawTimer, logTimer, logService))
            {
                parkingService.LogTimer.Start();
                parkingService.WithDrawTimer.Start();
                /**LVL = 1**/
                while (LVL == 1)
                {
                    Console.Clear();
                    ConsoleTextFormater.ShowMenuContent("PARKING SERVICE", new string[]{
                        "1. Parking balance",
                        "2. All parking balance",
                        "3. Free places",
                        "4. Current transactions",
                        "5. History of transactions",
                        "6. List of vehicles",
                        "7. Add vehicle",
                        "8. Remove vehicle",
                        "9. Top up vehicle",
                        "0. Back"
                    });
                    string point = ConsoleTextFormater.UserConsoleEnter("point");
                    DoAction(point);
                }
            };
        }

        private void ShowSettings()
        {
            /**LVL = 2**/
            while (LVL == 2)
            {
                Console.Clear();
                ConsoleTextFormater.ShowMenuContent("SETTINGS", new string[]{
                    $"1. Balance: {Settings.Balance}",
                    $"2. Capacity: {Settings.Capacity}",
                    $"3. Payment period: {Settings.PaymentPeriod}",
                    $"4. Log write period: {Settings.LogWritePeriod}",
                    $"5. Fine factor: {Settings.FineFactor}",
                    "6. Tariffes",
                    "0. Back"
                });
                string point = ConsoleTextFormater.UserConsoleEnter("point").Trim();
                DoSettings(point);
            }
        }

        private void DoMenu(string point)
        {
            switch (point)
            {
                case "1":
                    LVL = 1;
                    StartService();
                    break;
                case "2":
                    LVL = 2;
                    ShowSettings();
                    break;
                case "3":
                    LVL = -1;
                    break;
                default:
                    break;
            }
        }

        private void DoAction(string point)
        {
            Console.ForegroundColor = ConsoleColor.Magenta;
            try
            {
                switch (point)
                {
                    case "1":
                        ShowParkingBalance();
                        ConsoleTextFormater.UserAwaiting();
                        break;
                    case "2":
                        ShowCurrentParkingBalance();
                        ConsoleTextFormater.UserAwaiting();
                        break;
                    case "3":
                        ShowFreePlaces();
                        ConsoleTextFormater.UserAwaiting();
                        break;
                    case "4":
                        ShowCurrentParkingTransactions();
                        ConsoleTextFormater.UserAwaiting();
                        break;
                    case "5":
                        ShowLogParkingTransactions();
                        ConsoleTextFormater.UserAwaiting();
                        break;
                    case "6":
                        ShowCurrentVehicles();
                        ConsoleTextFormater.UserAwaiting();
                        break;
                    case "7":
                        AddVehicle();
                        break;
                    case "8":
                        RemoveVehicle();
                        break;
                    case "9":
                        TopUpVehicle();
                        break;
                    case "0":
                        LVL = 0;
                        break;
                    default:
                        break;
                }
            }
            catch (InvalidOperationException Ex)
            {
                ConsoleTextFormater.ShowErrorMessage(Ex.Message);
                ConsoleTextFormater.UserAwaiting();
            }
            catch (ArgumentException Ex)
            {
                ConsoleTextFormater.ShowErrorMessage(Ex.Message);
                ConsoleTextFormater.UserAwaiting();
            }
        }

        private void DoSettings(string point)
        {
            try
            {
                switch (point)
                {
                    case "1":
                        string value = ConsoleTextFormater.UserConsoleEnter("new value").Trim();
                        var numb = TryParseFunctions.TryReadDecimal(value);
                        if (numb < 0)
                            throw new ArgumentException("Incorrect argument. Must be >= 0.");
                        Settings.Balance = numb;
                        break;
                    case "2":
                        value = ConsoleTextFormater.UserConsoleEnter("new value").Trim();
                        Settings.Capacity = TryParseFunctions.TryReadUShortPositive(value);
                        break;
                    case "3":
                        value = ConsoleTextFormater.UserConsoleEnter("new value").Trim();
                        Settings.PaymentPeriod = TryParseFunctions.TryReadDoublePositive(value);
                        break;
                    case "4":
                        value = ConsoleTextFormater.UserConsoleEnter("new value").Trim();
                        Settings.LogWritePeriod = TryParseFunctions.TryReadDoublePositive(value);
                        break;
                    case "5":
                        value = ConsoleTextFormater.UserConsoleEnter("new value").Trim();
                        Settings.FineFactor = TryParseFunctions.TryReadDoublePositive(value);
                        break;
                    case "6":
                        ConsoleTextFormater.ShowMessage("Types:", ConsoleColor.DarkCyan);
                        foreach (var type in Settings.VehicleTariffes)
                            ConsoleTextFormater.ShowMessage($"{(int)type.Key}. {type.Key.ToString()}: {type.Value.ToString()}", ConsoleColor.Cyan, addLeft: 4);
                        var vehicleType = TryParseFunctions.TryReadUShortPositive(ConsoleTextFormater.UserConsoleEnter("vehicle type number"));
                        if (!(Enum.IsDefined(typeof(VehicleType), vehicleType)))
                        {
                            ConsoleTextFormater.ShowErrorMessage("Not existing type!");
                            break;
                        }
                        Settings.VehicleTariffes[(VehicleType)vehicleType] = TryParseFunctions.TryReadDoublePositive(
                            ConsoleTextFormater.UserConsoleEnter("new value").Trim());
                        break;
                    case "0":
                        LVL = 0;
                        break;
                    default:
                        break;
                }
            }
            catch (ArgumentException ex)
            {
                ConsoleTextFormater.ShowErrorMessage(ex.Message);
                ConsoleTextFormater.UserAwaiting();
            }
        }

        private void TopUpVehicle()
        {
            ConsoleTextFormater.ShowTopicMessage("Top up vehicle balance:");
            string vehicleId = ConsoleTextFormater.UserConsoleEnter("vehicle ID");
            decimal sum = TryParseFunctions.TryReadDecimal(ConsoleTextFormater.UserConsoleEnter("vehicle balance"));
            parkingService.TopUpVehicle(vehicleId, sum);
        }

        private void RemoveVehicle()
        {
            ConsoleTextFormater.ShowTopicMessage("Removing Vehicle:");
            string vehicleId = ConsoleTextFormater.UserConsoleEnter("vehicle ID");
            parkingService.RemoveVehicle(vehicleId);
        }

        private void AddVehicle()
        {
            /** If not supported balance, balance = 0 **/
            ConsoleTextFormater.ShowTopicMessage("Adding vehicle:"); 
            ConsoleTextFormater.ShowMessage("Types:", ConsoleColor.DarkCyan);
            foreach (var type in Settings.VehicleTariffes)
                ConsoleTextFormater.ShowMessage($"{(int)type.Key}. {type.Key.ToString()}", ConsoleColor.Cyan, addLeft: 4);
            var vehicleType = TryParseFunctions.TryReadUShort(ConsoleTextFormater.UserConsoleEnter("vehicle type number"));
            if (!(Enum.IsDefined(typeof(VehicleType), vehicleType)))
                throw new InvalidOperationException("Not existing type!");
            decimal balance = TryParseFunctions.TryReadDecimal(ConsoleTextFormater.UserConsoleEnter("vehicle balance"));
            parkingService.AddVehicle(new Vehicle(Vehicle.GenerateRandomRegistrationPlateNumber(), (VehicleType)vehicleType, balance));
        }

        private void ShowCurrentVehicles()
        {
            ConsoleTextFormater.ShowTopicMessage("Vehicles in the parking:");
            var vehicles = parkingService.GetVehicles();
            if (vehicles.Count == 0)
                ConsoleTextFormater.ShowMessage("No vehicles!", ConsoleColor.Blue);
            foreach (var veh in vehicles)
                ConsoleTextFormater.ShowMessage(ConsoleTextFormater.GetVehicleInfo(veh), addLeft: 4);
        }

        private void ShowLogParkingTransactions()
        {
            ConsoleTextFormater.ShowTopicMessage("Parking transaction history:");
            var transactions = parkingService.ReadFromLog().Split("\n");
            if (transactions.Length == 0)
                ConsoleTextFormater.ShowMessage("No transactions!", ConsoleColor.Blue);
            else
                for (int i = 0; i < transactions.Length; ++i)
                    ConsoleTextFormater.ShowMessage(transactions[i], addLeft:4);
        }

        private void ShowCurrentParkingTransactions()
        {
            ConsoleTextFormater.ShowTopicMessage("Current parking transaction:");
            var transactions = parkingService.GetLastParkingTransactions();
            if (transactions.Length == 0)
                ConsoleTextFormater.ShowMessage("No transactions!", ConsoleColor.Blue);
            for (int i = 0; i < transactions.Length; ++i)
                ConsoleTextFormater.ShowMessage(transactions[i].ToString(), addLeft:4);
        }

        private void ShowFreePlaces()
        {
            ConsoleTextFormater.ShowTopicMessage("Free places");
            ConsoleTextFormater.ShowGoodMessage($"{parkingService.GetFreePlaces()}/{Settings.Capacity}");
        }

        private void ShowCurrentParkingBalance()
        {
            ConsoleTextFormater.ShowTopicMessage("Parking current balance");
            decimal sum = 0;
            try
            {
                string[] transactions = parkingService.ReadFromLog().Split("\n");
                foreach (var trans in transactions)
                {
                    var str = trans.Split(":");
                    sum += TryParseFunctions.TryReadDecimal(str[str.Length-1].Trim());
                }
            }
            catch (InvalidOperationException)
            { }
            ConsoleTextFormater.ShowGoodMessage($"{parkingService.GetBalance()-sum}");
        }

        private void ShowParkingBalance()
        {
            ConsoleTextFormater.ShowTopicMessage("Parking balance");
            ConsoleTextFormater.ShowGoodMessage($"{parkingService.GetBalance()}");
        }
    }
}
