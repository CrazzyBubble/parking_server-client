﻿namespace CoolParking.UserInterface
{
    class Program
    {
        static void Main(string[] args)
        {
            /** Вся парковка починає працювати при виборі пункта 1. Start
             * При виборі пункта 2. Settings відкриваються налаштування
             * При виборі пунтка 0. Back можна повернутися до початкового меню,
             * робота парковки оновиться.
             * При кожному початку роботи log файл видаляється.**/

            string _logFilePath = $@".\Transactions.log";
            ConsoleUserInterface serviceInteraction = new ConsoleUserInterface(_logFilePath);
            serviceInteraction.Run();
        }
    }
}
