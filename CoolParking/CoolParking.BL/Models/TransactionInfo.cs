﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.

using System;
using Newtonsoft.Json;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        [JsonProperty("sum")]
        public decimal Sum { get; }

        [JsonProperty("vehicleId")]
        public string VehicleID { get; }

        [JsonProperty("transactionDate")]
        public DateTime TransactionDate { get; }

        public TransactionInfo(decimal sum, string vehicleID, DateTime time)
        {
            Sum = sum;
            VehicleID = vehicleID;
            TransactionDate = time;
        }
        public override string ToString()
        {
            return String.Format("{0:HH:mm:ss}", TransactionDate) + $"\t{VehicleID}: {Sum}";
        }
    }
}