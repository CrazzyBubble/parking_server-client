﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal Balance { set; get; } = 0;
        public static int Capacity { set; get; } = 10;
        public static double PaymentPeriod { set; get; } = 5;
        public static double LogWritePeriod { set; get; } = 60;
        public static double FineFactor { set; get; } = 2.5;
        public static Dictionary<VehicleType, double> VehicleTariffes = new Dictionary<VehicleType, double> {
            { VehicleType.Bus, 3.5 },
            { VehicleType.Motorcycle,1 },
            { VehicleType.PassengerCar, 2 },
            { VehicleType.Truck, 5 }
        };
    }
}