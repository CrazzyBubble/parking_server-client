﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using System;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("vehicleType")]
        public VehicleType VehicleType { get; set; }

        [JsonProperty("balance")]
        public decimal Balance { /*internal*/ set; get; }

        public Vehicle(string _id, VehicleType _vehicleType, decimal _balance)
        {
            if (!IsTrueId(_id))
                throw new ArgumentException("Incorrect ID.");
            if (_balance <= 0)
                throw new ArgumentException("Not enough money.");
            if ((int)_vehicleType<1 || (int)_vehicleType > 4)
                throw new ArgumentException("Error vehicle type.");
            Id = _id;
            VehicleType = _vehicleType;
            Balance = _balance;
        }

        public Vehicle(){}

        public static string GenerateRandomRegistrationPlateNumber()
        {
            //return Guid.NewGuid().ToString("N");
            string format = "XX-YYYY-XX";
            string letters = "abcdefghijklmnopqrstuvwxyz".ToUpper();
            string numbers = "0123456789";
            string result = "";
            var random = new Random();
            for (int i = 0; i < format.Length; ++i)
            {
                switch (format[i])
                {
                    case 'X':
                        result += letters[random.Next(letters.Length)];
                        break;
                    case 'Y':
                        result += numbers[random.Next(numbers.Length)];
                        break;
                    default:
                        result += format[i];
                        break;
                }
            }
            return result;
        }

        public static bool IsTrueId(string id)
        {
            string pattern = @"^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$";
            return Regex.IsMatch(id, pattern);
        }

        public override string ToString()
        {
            return $"{Id}\t {VehicleType}\t {Balance}";
        }
    }
}