﻿// TODO: implement enum VehicleType.
//       Items: PassengerCar, Truck, Bus, Motorcycle.

namespace CoolParking.BL.Models
{
    public enum VehicleType:int
    {
        Bus = 1,
        Motorcycle,
        PassengerCar,
        Truck
    }
}