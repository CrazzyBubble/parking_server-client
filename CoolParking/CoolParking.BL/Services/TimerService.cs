﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.

using CoolParking.BL.Interfaces;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private Timer timer;
        public double Interval { get { return timer.Interval / 1000; } set { timer.Interval = value*1000; } }

        public event ElapsedEventHandler Elapsed;

        public TimerService(double interval)
        {
            timer = new Timer();
            Interval = interval;
            timer.Elapsed += (obj, e) => Elapsed.Invoke(obj, e);
            timer.AutoReset = true;
            timer.Enabled = false;
        }

        
        public void Start()
        {
            timer.Start();
        }

        public void Stop()
        {
            timer.Stop();
        }
        public void Dispose()
        {
            timer.Dispose();
        }
    }
}