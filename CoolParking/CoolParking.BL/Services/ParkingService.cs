﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Text;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private Parking parking;
        private List<TransactionInfo> ParkingTransactions { set; get; }
        public ITimerService WithDrawTimer { get; }
        public ITimerService LogTimer { get; }
        public ILogService LogService { get; }

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            parking = Parking.getParking();
            parking.Vehicles.Clear();
            parking.Balance = Settings.Balance;
            ParkingTransactions = new List<TransactionInfo>();
            WithDrawTimer = withdrawTimer;
            LogTimer = logTimer;
            LogService = logService;
            LogTimer.Elapsed += (obj, e) => {
                var text = new StringBuilder();
                foreach (var trans in ParkingTransactions)
                    text.Append(trans.ToString() + "\n");
                LogService.Write(text.ToString());
                ParkingTransactions.Clear();
            };
            WithDrawTimer.Elapsed += (obj, e) =>
            {
                foreach (Vehicle veh in parking.Vehicles)
                {
                    decimal sum = GetWithdrawSum(veh);
                    veh.Balance -= sum;
                    parking.Balance += sum;
                    ParkingTransactions.Add(new TransactionInfo(sum, veh.Id, e?.SignalTime ?? DateTime.Now));
                }
            };
            LogTimer.Start();
            WithDrawTimer.Start();
        }
        public ParkingService() : this(
            new TimerService(Settings.PaymentPeriod), 
            new TimerService(Settings.LogWritePeriod), 
            new LogService(@".\Transactions.log")) { }
        private decimal GetWithdrawSum(Vehicle veh)
        {
            decimal tarif = (decimal)Settings.VehicleTariffes[veh.VehicleType];
            decimal fineFact = (decimal)Settings.FineFactor;
            if (veh.Balance >= tarif)
                return tarif;
            else if (veh.Balance <= 0)
                return tarif * fineFact;
            else
                return veh.Balance + (tarif - veh.Balance) * fineFact;
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (parking.Vehicles.Count >= Settings.Capacity)
                throw new InvalidOperationException("Full parking. Come later!");
            if (!(parking.getVehicleById(vehicle.Id) is null))
                throw new ArgumentException("Transport with this ID already exist.");
            parking.Vehicles.Add(vehicle);
        }

        public decimal GetBalance()
        {
            return parking.Balance;
        }

        public int GetCapacity()
        {
            return Settings.Capacity;
        }

        public int GetFreePlaces()
        {
            return Settings.Capacity - parking.VehicleCount;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return ParkingTransactions.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(parking.Vehicles);
        }

        public string ReadFromLog()
        {
            return LogService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            Vehicle vehicle = parking.getVehicleById(vehicleId) ?? throw new ArgumentException("Vehicle isn't found.");
            if (vehicle.Balance < 0)
                throw new InvalidOperationException($"Not enough money (VehicleId = {vehicleId}). Pay the debts!");
            parking.Vehicles.Remove(vehicle);
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum <= 0)
                throw new ArgumentException("Incorrect sum.");
            Vehicle vehicle = parking.getVehicleById(vehicleId) ?? throw new ArgumentException("Vehicle isn't found.");
            vehicle.Balance += sum;
            ParkingTransactions.Add(new TransactionInfo(sum, vehicle.Id, DateTime.Now));
        }

        public void Dispose()
        {
            parking.Vehicles.Clear();
            parking.Balance = Settings.Balance;
            WithDrawTimer.Stop();
            LogTimer.Stop();
            if (File.Exists(LogService.LogPath))
                File.Delete(LogService.LogPath);
            WithDrawTimer.Dispose();
            LogTimer.Dispose();
        }
    }
}