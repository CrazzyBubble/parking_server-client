﻿using Newtonsoft.Json;

namespace CoolParking.WebAPI.Models
{
    public class TopUp
    {
        [JsonProperty("id")]
        public string id { set; get; }

        [JsonProperty("Sum")]
        public decimal Sum { set; get; }
    }
}
