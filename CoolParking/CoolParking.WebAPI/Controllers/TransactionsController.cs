﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    public class TransactionsController : Controller
    {
        private IParkingService parkingService;

        public TransactionsController(IParkingService service)
        {
            parkingService = service;
        }

        // GET: api/<controller>/last
        [HttpGet("last")]
        public ActionResult<string> GetLastTransactions()
        {
            return Ok(JsonConvert.SerializeObject(parkingService.GetLastParkingTransactions()));
        }

        // GET: api/<controller>/all
        [HttpGet("all")]
        public ActionResult<string> GetAllTransactions()
        {
            try
            {
                return Ok(JsonConvert.SerializeObject(parkingService.ReadFromLog()));
            }
            catch (InvalidOperationException ex)
            {
                return NotFound(ex.Message);
            }
        }

        // PUT api/<controller>/topUpVehicle
        [HttpPut("topUpVehicle")]
        public ActionResult<string> PutTopUpVehicle([FromBody]TopUp topUp)
        {
            if (topUp is null)
                return BadRequest("Invalid vehicle data.");
            try
            {
                Console.WriteLine($"{topUp.id}, {topUp.Sum}");
                parkingService.TopUpVehicle(topUp.id, topUp.Sum);
            }
            catch (ArgumentException ex)
            {
                return NotFound(ex.Message);
            }
            return Ok(JsonConvert.SerializeObject(parkingService
                .GetVehicles()
                .Where(vehicle => vehicle.Id.Equals(topUp.id))
                .First()
                ));
        }
    }
}
