﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : Controller
    {
        private IParkingService parkingService;

        public ParkingController(IParkingService service)
        {
            parkingService = service;
        }

        // GET: api/<controller>/balance
        [HttpGet("balance")]
        public ActionResult<string> GetBalance()
        {
            return Ok(JsonConvert.SerializeObject(parkingService.GetBalance()));
        }

        // GET api/<controller>/capacity
        [HttpGet("capacity")]
        public ActionResult<string> GetCapacity()
        {
            return Ok(JsonConvert.SerializeObject(parkingService.GetCapacity()));
        }

        // GET api/<controller>/freePlaces
        [HttpGet("freePlaces")]
        public ActionResult<string> GetFreePlaces()
        {
            return Ok(JsonConvert.SerializeObject(parkingService.GetFreePlaces()));
        }
    }
}
