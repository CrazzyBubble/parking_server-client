﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : Controller
    {
        private IParkingService parkingService;

        public VehiclesController(IParkingService service)
        {
            parkingService = service;
        }

        // GET: api/<controller>
        [HttpGet]
        public ActionResult<string> GetVehicles()
        {
            return Ok(JsonConvert.SerializeObject(parkingService.GetVehicles()));
        }

        // GET api/<controller>/{id}
        [HttpGet("{id}")]
        public ActionResult<string> GetVehicle(string id)
        {
            if (!Vehicle.IsTrueId(id))
                return BadRequest("Invalid id, must be (example: AA-1111-AA)");
            var vehicles = parkingService.GetVehicles().ToList<Vehicle>();
            Vehicle vehicle = vehicles.Find(vehicle => vehicle.Id == id);
            if (vehicle is null)
                return NotFound("Vehicle not found!");
            return Ok(JsonConvert.SerializeObject(vehicle));
        }

        [HttpPost]
        public ActionResult<string> PostCreateVehicle([FromBody]Vehicle vehicle)
        {
            if (vehicle is null)
                return BadRequest("Invalid vehicle data.");
            try
            {
                var veh = new Vehicle(vehicle.Id, (VehicleType)vehicle.VehicleType, vehicle.Balance);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            try
            {
                parkingService.AddVehicle(vehicle);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch(InvalidOperationException ex)
            {
                return BadRequest(ex.Message);
            }
            return Created(Request.GetDisplayUrl(), JsonConvert.SerializeObject(vehicle));
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public ActionResult DeleteVehicle(string id)
        {
            if (!Vehicle.IsTrueId(id))
                return BadRequest("Invalid id, must be (example: AA-1111-AA)");
            try
            {
                parkingService.RemoveVehicle(id);
            }
            catch (ArgumentException ex)
            {
                return NotFound(ex.Message);
            }
            return NoContent();
        }
    }
}
